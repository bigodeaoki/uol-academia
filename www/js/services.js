var apre=null;

angular.module('starter.services', [])
  .factory('treinamentos', function () {
    treinamentos = [
      { titulo: 'Treinamento Kanban Basico', data: new Date(), inscrito: false },
      { titulo: 'Teste Agil', data: new Date(), inscrito: true },
      { titulo: 'Treinamento Teste Agil', data: new Date(), inscrito: false },
      { titulo: 'Treinamento', data: new Date(), inscrito: true },
      { titulo: 'Treinamento', data: new Date(), inscrito: false },
      { titulo: 'Treinamento', data: new Date(), inscrito: true },
      { titulo: 'Treinamento', data: new Date(), inscrito: false },
      { titulo: 'Treinamento', data: new Date(), inscrito: true },
      { titulo: 'Treinamento', data: new Date(), inscrito: false },
      { titulo: 'Treinamento', data: new Date(), inscrito: true },
    ];
    
    return treinamentos;
    
  })
  .factory('firebase',function(){
    return new firebaseHandler();
  })
  .factory('qrCode',function(firebase){
    return new qrCodeHandler(firebase);
  })
  .factory('treinamentosFeedback', function () {
    treinamentosFeedback = [
      { titulo: 'Treinamento Kanban Basico' , avaliado: true},
      { titulo: 'Treinamento Teste Agil'    , avaliado: false},
      { titulo: 'Treinamento'               , avaliado: true},
      { titulo: 'Treinamento'               , avaliado: false}
    ]
    return treinamentosFeedback;
  }
  ).factory('tecDayApre', function () {
    
    apre = [
      { 
        titulo: 'Inicio TechDay', 
        area: 'UOLHost',
        autor: "Eduardo Ramos",
        linguagem: "PHP",
        resumo: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio iusto quidem atque ratione, amet quaerat tenetur.",
        hora:"10:30 - 11:30"
      },
      { 
        titulo: 'Apresentação sobre algo', 
        area: 'PagSeguro',
        autor: "Eduardo Ramos",
        linguagem: "Java",
        resumo: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio iusto quidem atque ratione, amet quaerat tenetur.",
        hora:"11:30 - 12:30"
      },
      { 
        titulo: 'Almoço', 
        area: 'Intervalo',
        autor: "",
        linguagem: "PHP",
        resumo: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio iusto quidem atque ratione, amet quaerat tenetur.",
        hora:"12:30 - 13:30"
      },
      { 
        titulo: 'Ruby on Rails', 
        area: 'UOLHost',
        autor: "Eduardo Ramos",
        linguagem: "PHP",
        resumo: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio iusto quidem atque ratione, amet quaerat tenetur.",
        hora:"13:30 - 15:00"
      }
    ]
    return {apresentacoes:apre , data:"25/09"};
  });
