// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

makeBrowserDeviceConfig=function(){
  if(getCookie("uuid")==""){
          setCookie("uuid",Math.floor(Date.now()),1000);
        }

        var uuid=getCookie("uuid");

        Math.floor(Date.now() / 1000)

        window.device = {
          platform: "browser",
          model: encodeURI(navigator.platform),
          uuid: uuid
  }
}


angular.module('starter', 
  [
    'ionic', 
    'starter.controllers',
    'starter.AdminControllers',
    'starter.AdminFeedbackControllers',
    'starter.AdminPerguntaControllers',
    'starter.services'
  ]
  )

  .run(function ($ionicPlatform,firebase) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
      try{
        console.log("Validando Device"+device);
        if(device.platform=='browser'){
          makeBrowserDeviceConfig();  
        }
      }catch(err){
        console.log("Device não encontrado. Ativando mock");
        makeBrowserDeviceConfig();
      }

      // Start firebase for home
      firebase.init();
    });
  })
  .constant("CONFIG",{
    'passwd': 'UOL@2017'
  })
  .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    


    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      // setup an abstract state for the tabs directive
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html',
        controller: 'TesteController'
      })
      .state('tab.agenda', {
        url: '/agenda',
        views: {
          'tab-agenda': {
            templateUrl: 'templates/tab-agenda.html',
            controller: 'agendaCtrl'
          }
        }
      })

      .state('tab.feedback', {
        url: '/feedback',
        views: {
          'tab-feedback': {
            templateUrl: 'templates/tab-feedback.html',
            controller: 'feedbackListCtrl'
          }
        }
      })
      .state('tab.tecDay', {
        url: '/tecDay',
        views: {
          'tab-tecDay': {
            templateUrl: 'templates/tab-tecDay.html',
            controller: 'tecDayCtrl'
          }
        }
      })
      .state('tab.workshop', {
        url: '/workshop',
        views: {
          'tab-tecDay': {
            templateUrl: 'templates/tab-workshop.html',
            controller: 'tecWorkshop'
          }
        }
      })
      .state('tab.question', {
        url: '/perguntas',
        views: {
          'tab-question': {
            templateUrl: 'templates/perguntas/make.html',
            controller: 'QuestionCtrl'
          }
        }
      })

      .state('tab.presenca', {
        url: '/presenca',
        views: {
          'tab-account': {
            templateUrl: 'templates/tab-presenca.html',
            controller: 'PresencaCtrl'
          }
        }
      })
      .state('tab.admin', {
        url: '/admin',
        views: {
          'tab-admin': {
            templateUrl: 'templates/admin/home.html',
            controller: 'AdminControllers'
          }
        }
      })

      .state('tab.admin2', {
        url: '/admin2',
        views: {
          'tab-admin2': {
            templateUrl: 'templates/admin/home2.html',
            controller: 'AdminControllers'
          }
        }
      })
      .state('tab.admin3', {
        url: '/admin-feedback',
        views: {
          'tab-admin3': {
            templateUrl: 'templates/admin/feedbacks.html',
            controller: 'AdminFeedbackControllers'
          }
        }
      })

      .state('feedback',{
        cache: false,
        url: '/feedback/:id/:title',
        templateUrl: 'templates/feedback.html'
      })
      .state('home', {
        url: '/home',
        templateUrl: 'templates/home/home.html',
        controller: 'HomeCtrl'
      })
      .state('admin', {
        url: '/admin',
        templateUrl: 'templates/admin/home.html',
        controller: 'AdminControllers'
      })
      ;

    // if none of the above states are matched, use this as the fallback
    //$urlRouterProvider.otherwise('/tab/admin');
    $urlRouterProvider.otherwise('/tab/workshop');

  })

  .directive('select',function(){ //same as "ngSelect"
    return {
        restrict: 'E',
        scope: false,
        link: function (scope, ele) {
            ele.on('touchmove touchstart',function(e){
                e.stopPropagation();
            })
        }
    }
});
