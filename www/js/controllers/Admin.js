var app = angular.module('starter.AdminControllers', []);


app
.controller('AdminControllers', function($scope, $state, firebase, $ionicPopup, $timeout, $interval, $rootScope){

  $scope.listComplete = false;
  $scope.sorteioFinish = false;
  $scope.loadPalestras = false;
  $scope.showListaPerguntas = false;
  $scope.abacate = false;
  $scope.adminFeedback = false;

  $scope.data = {};
  $scope.sl={};

  var abacate;

  var counterSort = 0;

  $scope.setSl=function(arg){
    $scope.sl=arg;
    if(arg){
      Object.keys(arg).forEach(function(chave){
        var pergunta=arg[chave];
        pergunta.contadordevotos=0;
        if(pergunta.likes){
          Object.keys(pergunta.likes).forEach(function(deviceKey){
            var models=pergunta.likes[deviceKey];
            Object.keys(models).forEach(function(modelKey){
              var devicesKeys=models[modelKey];
              Object.keys(devicesKeys).forEach(function(deviceKey){
                pergunta.contadordevotos++;
              })
            })
          })
        } 
      })
    }
  }

  /*
  firebase.addInitCallback(function(){
    firebase.getApresentacaoes(function(arg){
      $scope.treinamentosFeedback = [];
      

      arg.forEach(function(current){
        if(current.titulo) $scope.treinamentosFeedback.push(current);
      });

      $scope.loadPalestras = true;

      if(!$scope.$$phase) {
        $scope.$apply();  
      }
    });
  });

  var setPerguntas = function (val) {
    $scope.listagemPerguntas = val;
    $scope.showListaPerguntas = !$scope.showListaPerguntas;
    if(!$scope.$$phase) {
      $scope.$apply();  
    }
  } 

  $scope.selectedItem = function (item) {
    if(angular.isDefined(item)) {
      firebase.getQuestions(item, setPerguntas);  
    }
  }
  */

  $scope.update = function () {
    
    $scope.listComplete = true;
    if (!$scope.$$phase) {
      $scope.$apply();  
    }

    /*
    if (!$rootScope.listaParticipantes) {

      $state.go('tab.workshop');

    } else {

      $scope.listComplete = true;
      if (!$scope.$$phase) {
        $scope.$apply();  
      }

    }
    
    /*
    if (!$scope.$$phase) {
      $scope.$apply();  
    }

    firebase.getSorteioInscriptos( function(arg) {  

      $scope.sorteio = {};

      arg.forEach(function(val) {
        
        $scope.sorteio[val.username] = val;

        if (!$scope.$$phase) {
          $scope.$apply();  
        }
        $scope.listComplete = true;

      });
      
    });
    */
  }

  $scope.mark = function(key,val) {
    firebase.markSorteado(key,val);
    $scope.update();
  }

  $scope.makeSorteio = function() {

    // Random Item Key For Sorteados
    var keys = Object.keys($rootScope.listaParticipantes);
    var luckyKey = keys[Math.floor(Math.random() * keys.length)];

    // Verificar se o usuário já foi sorteado
    if (!$rootScope.listaParticipantes[luckyKey].sorteado) {

      $rootScope.listaParticipantes[luckyKey].sorteado = true;
      $scope.luckyGuy = $rootScope.listaParticipantes[luckyKey];  
      
      $timeout( function () {

        $scope.sorteioFinish = true;
        if (!$scope.$$phase) {
          $scope.$apply();  
        }

      }, 150);

      var abacate = angular.copy($scope.luckyGuy);

      $scope.mark(abacate.key, abacate);

    } else {
      counterSort++;
      if ( counterSort < 100 ) {
        $scope.makeSorteio();
      } else {
        console.log("ERRO");
        $scope.sorteioFinish = false;
      }
    }
  }

  $scope.$on('$ionicView.beforeEnter', function(){
    // filtro
    $scope.update();

  });

  $scope.adicionarConvidado=function(){
    var presencaPopup = $ionicPopup.show({
      template: '<label>Nome: </label><br><input type="text" ng-model="data.user"> <label>Login: </label><input type="text" ng-model="data.username">',
      title: 'Digite Nome e Usuário UOL',
        // subTitle: 'sem @uolinc.com',
        scope: $scope,
        buttons: [
        { text: 'Cancelar' },
        {
          text: '<b>Salvar</b>',
          type: 'waves-light btn',
          onTap: function(e) {
            if (!$scope.data.user) {
                //don't allow the user to close unless he enters wifi password
                e.preventDefault();
              } else {
                
                var add = {user: $scope.data.user,username: $scope.data.username, manual:true};
                var check = true;

                for ( var i = 0 ; i < $rootScope.listaParticipantes.length; i++ ) {
                  if ( $rootScope.listaParticipantes[i].username == add.username ) {
                    check = false;
                  }
                }

                if (check) {
                  firebase.saveManualPresenca(add);
                  $rootScope.listaParticipantes.push(add); 
                } else {
                  console.log('Username jah inserido');
                }

                $scope.data.user = "";
                $scope.data.username = "";

                $scope.listComplete = false;
                $scope.update();
                return $scope.data;
              }
            }
          }
          ]
        });
  }

  var showPopUpConfirmationDelete = function () {  
    var confirmationPopUP = $ionicPopup.show({
      template: '<span style="text-align:center; display:block;">Usuário Deletado Com Sucesso</span>',
      title: 'Usuário Deletado!',
      scope: $scope,
      buttons: [
        {
          text: '<b>Fechar</b>',
          type: 'waves-light btn',
        }
      ]
    });
  }

  $scope.deleteEspertao = function (val) {
    var confirmationPopUP = $ionicPopup.show({
      template: '<span style="text-align:center; display:block;">Usuário Deletado Com Sucesso</span>',
      title: 'Usuário Deletado!',
      scope: $scope,
      buttons: [
        { text: 'Cancelar' },
        {
          text: '<b>Deletar</b>',
          type: 'waves-light btn',
          onTap: function (e) {
            firebase.deleteEspertao(val,showPopUpConfirmationDelete);
          }
        }
      ]
    });
  }

  /*
  for (var i = 0; i <1100; i++) {
    var add={user: 'user '+i ,username: 'username ' + i, manual:true};
    firebase.saveManualPresenca(add);
  }
  */
  
  $scope.changeAdminRoute = function (val) {
    if (val == 'perguntas') {
      $scope.adminFeedback = false;
    } else {
      $state.go(val);
    }
  }

})
;