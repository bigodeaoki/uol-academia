var app = angular.module('starter.AdminFeedbackControllers', []);

app
.controller('AdminFeedbackControllers', function($scope, firebase){
	
	var loadListaPalestras = function () {

		firebase.getApresentacaoes(function(arg){
      		$scope.treinamentosFeedback = [];
      
      		arg.forEach(function(current){
        		if(current.titulo) $scope.treinamentosFeedback.push(current);
      		});

      		$scope.loadPalestras = true;

      		if(!$scope.$$phase) {
        		$scope.$apply();  
      		}
    	});
	}

	var setFeedbacks = function (val) {

		var counter = 0;
		var nota = 0;
		var comentarios = [];
		$scope.comentarios = [];

		Object.keys(val).forEach( function(chave){
			Object.keys(val[chave]).forEach( function(id){
				
				var el = val[chave][id];

				if (el.nota != '' && el.nota != undefined) {
					counter++;
					nota = nota + parseInt(el.nota);

					obj = {};
					obj.id = id;
					obj.comentario = el.comentario;

					if (el.comentario != '' && el.comentario != undefined) {
						comentarios.push(obj);
					}
					
				}
			});
		});

		$scope.votosTotal = counter;
		$scope.notaTotal = nota;	
		$scope.comentarios = comentarios;
		if(!$scope.$$phase) {
    		$scope.$apply();  
  		}

	}

	$scope.selectPalestra = function (val) {

		$scope.nomePalestraSelecionada = val.split(';')[1];
		firebase.getFeedbackWorkshop(val.split(';')[0], setFeedbacks);
	}

	$scope.$on('$ionicView.beforeEnter', function(){
    	// filtro
    	if (!$scope.loadPalestras) {
    		// Reload Palestras
    		loadListaPalestras();
    	}
  	});

})