var app = angular.module('starter.AdminPerguntaControllers', []);

app
.controller('AdminPerguntaControllers', function($scope, firebase) {
	
	var loadListaPalestras = function () {
		firebase.getApresentacaoes(function(arg){
      		$scope.treinamentosFeedback = [];
      

      		arg.forEach(function(current){
        		if(current.titulo) $scope.treinamentosFeedback.push(current);
      		});

      		$scope.loadPalestras = true;

      		if(!$scope.$$phase) {
        		$scope.$apply();  
      		}
    	});
	}

	$scope.$on('$ionicView.beforeEnter', function(){
    	// filtro
    	if (!$scope.loadPalestras) {
    		// Reload Palestras
    		loadListaPalestras();
    	}
  	});

})
;