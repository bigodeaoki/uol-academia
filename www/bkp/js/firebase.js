var firebaseHandler=function(){
	var _self=this;
	this.config={workshop:{
		data:"14_12_2016",
		lastUpdate:"2016-12-07T20:00:16.614Z"
	}};
	this.initCallbacks=[];
	this.hasInit=false;

	var pathStart = "v3/workshop/";
	
	this.addInitCallback=function(callback){
		if(_self.hasInit) callback();
		else _self.initCallbacks.push(callback);
	}
	this.fireInitCallbacks=function(){
		_self.hasInit=true;
		_self.initCallbacks.forEach(function(arg){
			try{
				arg()
			}catch(err){
				console.log("Erro no callback "+arg);
			}
		});
	}
	this.fireCallbacks=function(){
		for(var j=0;j< _self.updateCallback.length;j++){
			try{
				_self.updateCallback[j]();
			}catch(err){
				console.log("Error in callback "+_self.updateCallback[j]);
			}
		}
	}

	this.init = function () {
		firebase.database().ref("v2/device/"+device.platform+"/"+device.model+"/"+device.uuid+"/feedback/").once('value').then(function(snapshot){
			snapshot.forEach(function(arg){
				// console.dir(arg.val());
				console.log("update feedback");
			})
		});
		firebase.database().ref("/v3/config").once("value").then(function(snapshot){
			_self.config=snapshot.val();
			console.log(_self.config);
			_self.fireInitCallbacks();
		});
	}

	this.getApresentacaoes=function(callback){
		firebase.database().ref("/v3/workshop/"+_self.config.workshop.data).once("value").then(function(snapshot){
			// callback(snapshot.val());
			var ret=[];
			snapshot.forEach(function(arg){
				var item=arg.val();
				item.key=arg.key;
				if(typeof item != "string")ret.push(item)
			});
			callback(ret);
		})
	}
	this.loadFeedbackWorkshop=function(id,callback){
		firebase.database()
				.ref("v3/workshop/"+_self.config.workshop.data+"/"+id+"/feedback/"+device.platform+"/"+device.model+"/"+device.uuid)
				.once('value')
				.then(function(snapshot) {
					callback(snapshot.val());
				}
		)
	}
	this.updateFeedbackWorkshop=function(id,arg){
		firebase.database().ref("v3/workshop/"+_self.config.workshop.data+"/"+id+"/feedback/"+device.platform+"/"+device.model+"/"+device.uuid)
		.update(arg);
	}

	this.savePresenca = function (path,obj) {
		
		firebase.database()
				.ref("v3/"+path+"/inscritos/"+device.platform+"/"+device.model+"/"+device.uuid)
				.update(obj);

		firebase.database()
				.ref("v3/"+path+"/sorteio/")
				.push(obj);
	}

	/*
	 * Method: GET
	 * Perguntas e Palestras
	 * 
	 */
		
		this.getPalestras = function (sendbackSelect) {

			var path = "v3/workshop/"+_self.config.workshop.data;
		 	var res = firebase.database()
		 		.ref(path);

		 	res.once('value', function(snapshot) {

		  			inscricao = snapshot.val();

		  			sendbackSelect(inscricao);

		 	});		
		}
		this.getQuestions = function (id, sendbackSelect) {

			var path = "v3/workshop/"+_self.config.workshop.data+"/"+id+"/perguntas";
		 	var res = firebase.database()
		 		.ref(path);

		 	res.once('value').then(function(snapshot) {

		 			var questions=[];
		  			snapshot.forEach(function(arg){
		  				var item=arg.val();
						item.key=arg.key;
						questions.push(item);
						if(myStorage.getItem('likes')!=null){
							if(JSON.parse(myStorage.getItem('likes')).indexOf(item.key)>=0){
								item.like=true;
							}
						}
		  			});
		  			console.log(questions);
		  			sendbackSelect(questions);

		 	});		
		}

		this.saveQuestions = function (id, obj, callback) {
			var path = pathStart + _self.config.workshop.data +"/"+ id + "/perguntas";
			var date = new Date();

			var txt = {
		 		type: "Pergunta",
		 		tpl: "Pergunta Enviada Com Sucesso"
		 	}

		 	obj.data = date.toLocaleTimeString() + " - " + date.toLocaleDateString();

		 	var id=firebase.database().ref().child( path ).push(obj);

		 	// myStorage.setItem('likes', $scope.data.usuario)

		 	

		 	// retornando mensagem
		 	callback (id, txt);

		}
		this.updateLike=function(palestraID,likeID){
			var pathToCount = pathStart + _self.config.workshop.data +"/"+ palestraID + "/perguntas/"+likeID+
			"/likes/"+device.platform+"/"+device.model+"/"+device.uuid;

			if(myStorage.getItem('likes')==null)myStorage.setItem('likes', JSON.stringify([]));
		 	var likes=JSON.parse(myStorage.getItem('likes'));
		 	likes.push(likeID);

		 	myStorage.setItem('likes', JSON.stringify(likes));

			firebase.database()
				.ref(pathToCount)
				.update({qtd: 1});

		}

	/*
	 * Sorteio de usuário presente
	 * Pegaremos a lista de presentes e sorteamos
	 * Usuário poderá ser sorteado apenas 1 vez
	 */
	 	var paths = {};
	 	paths.EligibleSorteio = "/v3/workshop/12_12_12/sorteio";
	 	paths.Sorteados = "/v3/workshop/12_12_12/sorteados";

		this.sorteio = function (callback) {

			this.getInscritos( callback );

		}


		this.saveManualPresenca=function(val){
			firebase.database()
				.ref("/v3/workshop/14_12_2016/sorteio").push(val);
				firebase.database()
				.ref("/v3/workshop/14_12_2016/inscritos/manual").push(val);
		}

		this.markSorteado=function(key,val){
			firebase.database()
				.ref("/v3/workshop/14_12_2016/sorteio/"+key).update(val);

		}

		this.getSorteioInscriptos=function(callback){
			// pegar inscritos
			firebase.database()
				.ref("/v3/workshop/14_12_2016/sorteio")
				.once("value")
				.then(function(snapshot){
					
					var ret=[];
					snapshot.forEach(function(arg){
						var item = arg.val();
						item.key = arg.key;
						if(typeof item != "string")ret.push(item)
					});

					callback(ret);

				});
		}

		this.getInscritos = function ( callback ) {
			// pegar inscritos
			firebase.database()
				.ref(paths.EligibleSorteio)
				.once("value")
				.then(function(snapshot){
					
					var ret=[];
					snapshot.forEach(function(arg){
						var item = arg.val();
						item.key = arg.key;
						if(typeof item != "string")ret.push(item)
					});

					_self.sortear( ret, callback);

				});

		}

		this.sortear = function (lista,callback) {

			console.log("length = " + lista.length);

			var index = Math.floor( Math.random() * (lista.length));
			this.getSorteado(lista,lista[index],callback);

		}

		this.getSorteado = function (lista, obj ,callback) {

			console.log(obj.nome);

			var res = firebase.database()
				.ref( paths.Sorteados )
				.orderByChild('nome')
				.equalTo( obj.nome);

			console.log("Pegar Dados de usuário Sorteado")
		 	res.once('value', function(snapshot) {
		 		
		 		console.log(snapshot.val());
		 		console.log("Acima valor do retorno da procura");
		 		console.log(typeof  snapshot.val() === 'object');

		 		if (snapshot.val() == null || snapshot.val() == undefined) {
		 			_self.saveSorteado(obj, callback);
		 		} else {
		 			_self.sortear(lista, callback);
		 		}

 			}, function (error){
 				
 				console.error(error);

 			});

		}

		this.getListaSorteados =  function (callback) {
			var res = firebase.database()
				.ref( paths.Sorteados )
				.orderByChild('nome');

			res.once('value', function(snapshot) {
				callback(snapshot.val());
			})
		}

		this.saveSorteado = function ( obj, callback) {
			
			var res = firebase.database()
				.ref( paths.Sorteados + '/' + obj.key)
				.update({nome:obj.nome});

			callback(obj);

		}		
	

	// this.apresentacoes=[];
	// this.updateCallback=[];

	// _self.path = {};




	// //console.log("Init firebase");
	// firebase.database().ref("v2/tecday/5_9_2016/").once('value').then(function(snapshot){
	// 	snapshot.forEach(function(arg){

	// 		var apresentacao=arg.val();
	// 		apresentacao.id=arg.key;

	// 		_self.apresentacoes.push(apresentacao);
	// 		_self.fireCallbacks();
	// 	})
	// })

	// this.addUpdateCallback=function(arg){
	// 	_self.updateCallback.push(arg);
	// 	arg();
	// }

	
	

	

	

	

	// this.updateSpeech=function( id,user ){
	// 	firebase.database()
	// 	        .ref("v2/tecday/5_9_2016/"+id+"/users/"+device.platform+"/"+device.model+"/"+device.uuid)
	// 	        .update({username:user});
	// }

	// // Inscriptions
	// this.startInscription = function (PalestraID, obj, callback) {
		
	// 	_self.path.saveInscricao 	= "v2/tecday/5_9_2016/"+PalestraID+"/inscricao";
	// 	_self.path.palestra			= "v2/tecday/5_9_2016/"+PalestraID;

	// 	this.getInscription(PalestraID, obj, this.saveInscription, callback);
	// }
	// this.saveInscription = function (obj, callback) {

	// 	console.log('Gravando Inscricao');

	// 	firebase.database().ref( _self.path.palestra ).once('value', function(snapshot){
			
	// 		var palestra = snapshot.val();

	// 		firebase.database().ref().child( _self.path.saveInscricao ).push(obj);
			
	// 		res = {
	// 			type: true,
	// 			msg: "Usuário Inscrito",
	// 			notification: {
	// 				//horario: palestra.hora,
	// 				horario: "16:15",
	// 				dia: '2016-11-18',
	// 				nome: palestra.titulo
	// 			}
	// 		}
	// 		callback(res);

	// 	});
	// }
	// this.getInscription = function (PalestraID, obj, successCallback, msgCallback) {
		
	// 	var res = firebase.database()
	// 		.ref(_self.path.saveInscricao)
	// 		.orderByChild('usuario')
	// 		.equalTo(obj.usuario);

	// 	res.once('value', function(snapshot) {
  			
 //  			inscricao = snapshot.val();

 //  			if (inscricao == null) {
 //  				successCallback(obj, msgCallback);
 //  			} else {
 //  				res = {
 //  					type: false,
 //  					msg: "Usuário jà Existente"
 //  				}
 //  				msgCallback(res);
 //  			}
	// 	});
	// }

	// this.getPaletras = function (sendbackSelect) {
		

	// 	var res = firebase.database()
	// 		.ref('v2/tecday/5_9_2016/');

	// 	res.once('value', function(snapshot) {
  			
 //  			inscricao = snapshot.val();

 //  			sendbackSelect(inscricao);

	// 	});		
	// }

	// this.saveQuestion = function (palestraId, question, callback) {

	// 	var path = "v2/tecday/5_9_2016/"+palestraId+"/perguntas";
	// 	var date = new Date();

	// 	var txt = {
	// 		type: "Pergunta",
	// 		tpl: "Pergunta Enviada Com Sucesso"
	// 	}

	// 	question.data = date.toLocaleTimeString() + " - " + date.toLocaleDateString();
	// 	firebase.database().ref().child( path ).push(question);
	// 	callback (palestraId, txt);

	// }

	// this.getQuestions = function (PalestraId, sendbackSelect) {

	// 	var path = 'v2/tecday/5_9_2016/' + PalestraId + '/perguntas';
	// 	console.log(path);
	// 	var res = firebase.database()
	// 		.ref(path);

	// 	res.once('value', function(snapshot) {
  			
 //  			perguntas = snapshot.val();

 //  			sendbackSelect(perguntas);

	// 	});		
	// }

	// this.saveLike = function (palestraId, questionId, sendbackSelect) {

	// 	var path = "v2/tecday/5_9_2016/"+palestraId+"/perguntas/"+questionId;
	// 	path += "/like";
	// 	path += "/"+device.platform+"/"+device.model+"/"+device.uuid;
		

	// 	arg = {
	// 		like: true
	// 	}

	// 	firebase.database().ref().child( path ).update(arg);

	// }

}