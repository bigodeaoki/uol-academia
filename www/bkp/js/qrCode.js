var qrCodeHandler=function(firebase){
	var _self=this;

	this.firebase=firebase;

	this.loadQRCode=function(username,callback){
    
    _self.callback = callback;
    
    _self.username = username;

    cordova.plugins.barcodeScanner.scan(
      
      _self.successScan,
      _self.failScan,
      {
        "preferFrontCamera" : false, // iOS and Android
        "showFlipCameraButton" : false, // iOS and Android
        "prompt" : "Place a barcode inside the scan area", // supported on Android only
      }

    );

	}

  this.successScan = function (result) {
    // firebase.updateSpeech( result.text, _self.username);
    console.log(result);
    if(!result.cancelled)_self.callback(200,result.text,_self.username);
  };

  this.failScan = function (result) {
    alert("Scanning failed: " + error);
    _self.callback(500);
  };

}
