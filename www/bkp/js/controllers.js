var ionicHistory=null;
var ionicNavBarDelegate=null;
var myStorage=null;
var abacate = null;

angular.module('starter.controllers', [])

.controller('BarraCtrl', function($scope,treinamentos,$ionicHistory) {
  
  $scope.backButton = function () {
    $ionicHistory.goBack();
  }

  myStorage = localStorage;

})
.controller('agendaCtrl', function($scope,treinamentos) {
  $scope.treinamentos=treinamentos;
})
.controller('feedbackCtrl', function(firebase,$scope,treinamentosFeedback, $stateParams,$location,$ionicHistory,$ionicNavBarDelegate,$ionicPopup) {
  
  $scope._notaPalestra=1;
  $scope.comentario="";

  $scope.$on ("$ionicView.beforeEnter", function(scopes, states) {
    var body = document.querySelector("body");
    body.classList.toggle('feedbackBody');
    $scope.title = $stateParams.title;
  });
  $scope.$on ("$ionicView.beforeLeave", function(scopes, states) {
    var body = document.querySelector("body");
    body.classList.toggle('feedbackBody');
  });

  $scope.showPopup = function() {
    
    var myPopup = $ionicPopup.show({
      template: '<p style="text-align: center;"> Agradecemos por ajudar a melhorar nossas palestras </p>',
      title: 'Obrigado pela sua avaliação!',
      scope: $scope,
      buttons: [
        {
          text: '<b>Obrigado <i class="ion-android-done-all"></i></b>',
          type: 'button-positive',
          onTap: function(e) {
            return true;
          }
        }
      ]
    });

  };


  // Funcoes Para a Avalicao da Palestra
  $scope.notaPalestra=function(arg){
  	if(angular.isDefined(arg)){
  		$scope._notaPalestra=arg;
  		$scope.update();
  		return $scope._notaPalestra;
  	}else{
  		return $scope._notaPalestra;
  	}
  };
  $scope.comentario=function(arg){
  	if(angular.isDefined(arg)){
  		$scope._comentario=arg;
  		$scope.update();
  		return $scope._comentario;
  	}else{
  		return $scope._comentario;
  	}
  };

  firebase.loadFeedbackWorkshop($stateParams.id,function(arg){
  	$scope.comentario(arg.comentario);
  	$scope.notaPalestra(arg.nota);
  	if(!$scope.$$phase) {
        $scope.$apply();  
    }
  })


  $scope.update=function(){
    var arg={
      nota: $scope.notaPalestra() ,
      comentario: $scope.comentario(),
    }
    if(!angular.isDefined(arg) || $scope._comentario==null )arg.comentario="";
    firebase.updateFeedbackWorkshop($stateParams.id,arg);    
  }


  $scope.feedbackGo=function(item){
  		$state.go("feedback");
	}
})
.controller('tecWorkshop',function(firebase,$scope,tecDayApre, $ionicPopup ){
  setTimeout(function(){
    firebase.addInitCallback(function(){
    firebase.getApresentacaoes(function(arg){
      $scope.apresentacoes=[];
      arg.forEach(function(current){
        if(current.titulo)$scope.apresentacoes.push(current)
        if(!$scope.$$phase) {
          $scope.$apply();  
        }
      });
    });
  });
  },1000)
})
.controller('tecDayCtrl', function(firebase,$scope,tecDayApre, $ionicPopup ) {
	
	/*  
	firebase.addUpdateCallback(function(){
		$scope.tecDayApre=firebase.apresentacoes;

		if(!$scope.$$phase) {
        	$scope.$apply();  
      	}
	});
	*/


  $scope.itemCLick=function(item){
  	item.show=!item.show;
  }

  $scope.mostrarResumo = function (item) {
    item.showResume = !item.showResume;
  }

  $scope.imgSrc = function (item) {

    if (item.area == 'pagseguro') {
      return "img/logo/pagseguro.jpg";
    } else if (item.area == 'jenkins') {
      return "img/logo/jenkins.jpg";
    } else if (item.area == 'splunk') {
      return "img/logo/splunk.jpg";
    } else if (item.area == 'kubernetes') {
      return "img/logo/kubernetes.jpg";
    } else if (item.area == 'coffee') {
      return "img/logo/coffee.jpg";
    } else if (item.area == 'gb') {
      return "img/logo/gb.jpg";
    } else {
      return "img/logo/uol.jpg";
    }

  };

  //Azul, amarelo, vermelho e verde
  $scope.selectClass = function(num) {
    if ((num % 4) == 0) {
      return "border-blue";
    } else if ((num % 4) == 1) {
      return "border-yellow";
    } else if ((num % 4) == 2) {
      return "border-red";
    } else if ((num % 4) == 3) {
      return "border-green";
    }
  }

  $scope.fazerInscricao = function (palestraID) {
    $scope.data = {};

    var nome = null;
    var templateTxt = '';

    if (myStorage.getItem('username')) {
      $scope.data.usuario = myStorage.getItem('username');
    } else {
      templateTxt += '<input type="text" ng-model="data.usuario" Placeholder="Nome">';
    } 

    templateTxt += '<input type="text" ng-model="data.gestor" Placeholder="Gestor">';

    var myPopup = $ionicPopup.show({
      template: templateTxt,
      title: 'Dados para inscrição',
      subTitle: 'usuário e gestor',
      scope: $scope,
      buttons: [
        { text: 'Cancelar' },
        {
          text: '<b>Inscrever-se</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.data.usuario || !$scope.data.gestor) {
              //don't allow the user to close unless he enters wifi password
              e.preventDefault();
            } else {

              // persistindo no localstorage
              myStorage.setItem('username', $scope.data.usuario); 
              // Do inscription
              firebase.startInscription(palestraID, $scope.data, inscricaoExistente);

            }
          }
        }
      ]
    });

    var inscricaoExistente = function (res) {
      
      $scope.scheduler();
      // Popup para mostrar msg de confirmação ou de alerta
      console.dir(res);
      if (res.type == true) {
        
        $ionicPopup.show({
          template: '',
          title: res.msg,
          scope: $scope,
          buttons: [
            { text: 'obrigado!' },
          ]
        });

        //Agendar
        $scope.scheduler(res);

      } else {
        $ionicPopup.show({
          template: '',
          title: res.msg,
          subTitle: 'inscrição não realizada',
          scope: $scope,
          buttons: [
            { text: 'fechar' },
          ]
        });
      }

    }

    $scope.scheduler = function (res) {
        
        // Agendar uma notificacao
        //var time          = res.notification.dia + ' ' + res.notification.hora
        var time          = "2016-11-18 16:20";
        var scheduleTime  = new Date(time);

        console.log("ativar schedule");
        
        try {

          cordova.plugins.notification.local.schedule({
              text: res.notification.nome,
              at: scheduleTime,
              led: "FF0000",
              sound: null
          });

        } catch (e) {
          console.error('Error no Scheduler: ' + e);
        }

        console.log("schedule terminado");
    }

  }
})
.controller('feedbackListCtrl', function(firebase,$scope,treinamentosFeedback,$state) {
  firebase.addInitCallback(function(){
    firebase.getApresentacaoes(function(arg){
      $scope.treinamentosFeedback=[];

      arg.forEach(function(current){
        if(current.titulo) $scope.treinamentosFeedback.push(current);
      })


      console.log(arg)
      if(!$scope.$$phase) {
        $scope.$apply();  
      }
    });
  });
})
.controller('HomeCtrl', function($scope,treinamentosFeedback,$state,$location) {

  $scope.enterEvent = function (url) {
    $state.go(url);
  }
})

.controller('QuestionCtrl', function($scope, $state, firebase, $ionicPopup){
  
  	$scope.data = {};
  	$scope.procura = false;
  	$scope.loadPalestras = false;

  	// Ionic Events
	$scope.$on('$ionicView.beforeEnter', function($scope){
		// zerando o valor
		var templateTxt = "";   
  	});
	
	// Pegar as palestras cadastradas
	firebase.getPalestras(function(item){

		$scope.palestras = item;



		templateTxt = "";
		$scope.loadPalestras = true;
		$scope.procura = false;

		if(!$scope.$$phase) {
			$scope.$apply();  
		}
	});  	
  

	// Pegar perguntas cadastradas
  	var getPerguntas = function (id) {

    	firebase.getQuestions(id, function(item){
      
	      	$scope.perguntas = item;
	      	$scope.procura = true;
	        
	      	if(!$scope.$$phase) {
	        	$scope.$apply();  
	      	}
    	});
  	}

  $scope.doLike=function(item){
    firebase.updateLike(item.palestra,item.key)
    item.like=true;
    console.log(item);
    getPerguntas(item.palestra);
    if(!$scope.$$phase) {
      $scope.$apply();  
    }
  }

  $scope.selectedItem = function (item) {
    

    if(angular.isDefined(item)){

      getPerguntas(item);

      $scope.data.palestra = item;
      if(!$scope.$$phase) {
        $scope.$apply();  
      }
      $scope.hasPalestraSelect=($scope.data.palestra!=0);
      console.log("====>  "+ $scope.hasPalestraSelect);
      return $scope.data.palestra;
    } else {
      if(!$scope.$$phase) {
        $scope.$apply();  
      }
      return $scope.data.palestra;
    }
  }

  // pop question
  $scope.makeQuestion = function () {
    var templateTxt = "";   
    // An elaborate, custom popup
    if (myStorage.getItem('user')) {
      $scope.data.usuario = myStorage.getItem('user');
    } else {
      templateTxt += '<input type="text" ng-model="data.usuario" Placeholder="Nome">';
    }
    templateTxt += '<textarea ng-model="data.pergunta" style="width: 100%; min-height: 90px; border:1px solid #e0e0e0;"></textarea>';
    templateTxt += '<p style="margin-top: 5px;"><input type="checkbox" id="anonimo" ng-model="data.anonimo" /><label for="anonimo">Esconder Nome?</label></p>';

    $scope.myPopup = $ionicPopup.show({
      template: templateTxt,
      title: 'Escreva a pergunta',
      scope: $scope,
      buttons: [
        { text: 'Cancelar',
        onTap: function(e){
          
        }
      },
        {
          text: '<b>Enviar</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.data.pergunta || !$scope.data.palestra) {
              //don't allow the user to close unless he enters wifi password
              console.dir($scope.data);
              e.preventDefault();

            } else {
              try{
                firebase.saveQuestions($scope.data.palestra, $scope.data, popupCallback);
              }catch(err){console.log(err)}  
              $scope.myPopup.close();
              templateTxt = "";
              $scope.data.pergunta = "";
              getPerguntas($scope.data.palestra);
            }
          }
        }
      ]
    });

  }

  // Callback
  var popupCallback = function (id, txt){

    var alertPopup = $ionicPopup.alert({
      title: txt.type,
      template: txt.template
    });

    $scope.selectedItem(id);
  }



  // add like
  $scope.addLike = function (palestraId, perguntaId) {
    
    firebase.saveLike(palestraId, perguntaId,popupCallback);

  }
})
.controller('PresencaCtrl', function($scope, $state, firebase, $ionicPopup,qrCode){
	$scope.code=[];
  $scope.posi=0;

  $scope.add=function(val){
    if($scope.posi<=3){
      $scope.code[$scope.posi]=val;
      $scope.posi++;  
    }
    
  }

  $scope.rm=function(){
    if($scope.posi>0){
      $scope.posi--;
      $scope.code[$scope.posi]='';
    }
    
  }



    $scope.showConfirmation = function() {

      $scope.data = {};
      var code = '';
      

      if(typeof myStorage.getItem('username')=='undefined' || myStorage.getItem('username')==null)myStorage.setItem('username','');
      if(typeof myStorage.getItem('user')=='undefined' || myStorage.getItem('user')==null)myStorage.setItem('user','');

      console.log("");



      

        console.log('Usuario nao existente');
        // An elaborate, custom popup
        var presencaPopup = $ionicPopup.show({
          template: '<label>Nome: </label><br>'+
                    '<input type="text" ng-model="data.user">'+ 
                    '<label>Login: </label><input type="text" ng-model="data.username">'+
                    '<div class="row row-center" style="margin:5px">'  +
                      '<div class="col">'+
                        '<label> Codigo:  </label>'+
                      '</div>'+
                      '<div class="col">{{code[0]}}{{code[1]}}{{code[2]}}{{code[3]}}</div>'+
                    '</div>'+
                    '<div class="row row-center" style="margin:5px">'+
                      '<div class="col col-center"><button" ng-click="add(1)" class="waves-effect waves-light btn">1</button></div>'+
                      '<div class="col col-center"><button" ng-click="add(2)" class="waves-effect waves-light btn">2</button></div>'+
                    '</div>'+
                    '<div class="row row-r" style="margin:5px">'+
                      '<div class="col col-center"><button" ng-click="add(3)" class="waves-effect waves-light btn">3</button></div>'+
                      '<div class="col col-center"><button" ng-click="add(4)" class="waves-effect waves-light btn">4</button></div>'+
                    '</div>'+
                    '<div class="row row-center" style="margin:5px">'+
                    '<div class="col"></div>'+
                      '<div class="col" ><button ng-click="rm()" class="waves-effect waves-light btn">'+
                        'X'+
                      '</button></div>'+
                    '</div>'
                    ,
          title: 'Digite o seu Nome e Usuário UOL',
          // subTitle: 'sem @uolinc.com',
          scope: $scope,
          buttons: [
            { text: 'Cancelar' },
            {
              text: '<b>Salvar</b>',
              type: 'waves-light btn',
              onTap: function(e) {
                if (!$scope.data.user) {
                  //don't allow the user to close unless he enters wifi password

                  e.preventDefault();
                } else {
                  if($scope.code[0]==3 && $scope.code[1]==1 && $scope.code[2]==2 && $scope.code[3]==1){
                    $scope.code=[];
                    $scope.posi=0;
                    return 200;  
                  }else{
                    $scope.code=[];
                    $scope.posi=0;
                    return 500;
                  }
                  ;
                }
              }
            }
          ]
        });
        
        presencaPopup.then(function(res) {
          if (res==200) {
            firebase.savePresenca('workshop/14_12_2016',$scope.data); 
          }
          showConfirmationDialog(res);          
        });


    };

    showConfirmationDialog = function (code) {
      console.log(code);
      if (code && code < 201) {
        
        var confirmationPopUP = $ionicPopup.show({
            template: '<span style="text-align:center; display:block;">Obrigado por Confirmar sua presença</span>',
            title: 'Presença Confirmada',
            scope: $scope,
            buttons: [
              {
                text: '<b>Fechar</b>',
                type: 'waves-light btn',
              }
            ]
        });

      } else {

        var confirmationPopUP = $ionicPopup.show({
            template: '<span style="text-align:center; display:block;">Erro ao tentar confirmar, Por favor tentar novamente</span>',
            title: 'Erro!',
            scope: $scope,
            buttons: [
              {
                text: '<b>Fechar</b>',
                type: 'waves-light btn',
              }
            ]
        });

      }
    }
})
.controller('AdminCtrl', function($scope, $state, firebase, $ionicPopup){

  	// Vamos fazer o Sorteio
  	// Exmplo Workshop

  	$scope.$on('$ionicView.beforeEnter', function($scope){
    	// precisamos verificar se o usuário pode acessar essa página
  	});

    var showGanhador = function (item) {
      console.dir(item);
      var alertPopup = $ionicPopup.alert({
        title: 'Ganhador',
        template: '<b>' + item.nome + '</b>'
      });
    }

  	$scope.sorteio = function () {

  		firebase.sorteio(showGanhador);

  	}
})
;
