var firebase = require("firebase");
var HashTable = require('hashtable');
var Set = require('simple-hashset');

firebase.initializeApp({
	serviceAccount: "uolAcademiaTeste-1651fbf9daf3.json",
	databaseURL: "https://uolacademiateste.firebaseio.com"
});

var db = firebase.database();

exports.getTecDays=function(callback){
	var tecDayKeys=[];
	db.ref("v2/tecday/").ref.once("value", function(snapshot) {
    	snapshot.forEach(function(arg){
    		tecDayKeys.push(arg.key);
    	});
    	callback(tecDayKeys);
	});
}

exports.getTecDay=function(idTecDay,callback){
	var palestras=[];

	db.ref("v2/tecday/"+idTecDay).ref.once("value",function(snapshot){
		snapshot.forEach(function(arg){
			var item=arg.val();
			item.key=arg.key;
			// item.feedback="/feedback";
			// item.users="/users";
			palestras.push(item);
		});
		callback(palestras);
	})
}

exports.getPalestra=function(idTecDay,idPalestra,callback){
	db.ref("v2/tecday/"+idTecDay+"/"+idPalestra).ref.once("value",function(snapshot){
		var item=snapshot.val();
		item.feedback="/feedback";
		item.users="/users";
		callback(item);
	})
}

exports.getPalestraFeedback=function(idTecDay,idPalestra,callback){
	db.ref("v2/tecday/"+idTecDay+"/"+idPalestra+"/feedback").ref.once("value",function(snapshot){
		var item=snapshot.val();

		var ret=[];
		if(item){
			if(item.Android){
				Object.keys(item.Android).forEach(function(element){
					Object.keys(item.Android[element]).forEach(function(com){
						ret.push(item.Android[element][com]);
					})
				})
			}

			if(item.ios){
				Object.keys(item.ios).forEach(function(element){
					Object.keys(item.ios[element]).forEach(function(com){
						ret.push(item.ios[element][com]);
					})
				})
			}
		}
		


		callback(ret);
	})
}

exports.getPalestraUsers=function(idTecDay,idPalestra,callback){
	db.ref("v2/tecday/"+idTecDay+"/"+idPalestra+"/users").ref.once("value",function(snapshot){
		var item=snapshot.val();
		var ret=[];
		if(item){
			if(item.Android){
				Object.keys(item.Android).forEach(function(element){
					Object.keys(item.Android[element]).forEach(function(com){
						ret.push(item.Android[element][com]);
					})
				})
			}

			if(item.ios){
				Object.keys(item.ios).forEach(function(element){
					Object.keys(item.ios[element]).forEach(function(com){
						ret.push(item.ios[element][com]);
					})
				})
			}
		}
		


		callback(ret);
	})
}

exports.addPalestra=function(idTecDay,palestra,callback){
	palestra.id=db.ref("v2/tecday/"+idTecDay).ref.push(palestra);
}

exports.addTecDay=function(idTecDay,callback){
	db.ref("v2/tecday/"+idTecDay).ref.once("value",function(snapshot){
		console.log("======> "+snapshot.val());
		if(snapshot.val()==null){
			db.ref("v2/tecday/"+idTecDay).update({criacao:new Date()});
			callback([snapshot.val()]);
		}else{
			callback(null);	
		}		
	})
}

exports.setWorkshop=function(data,callback){
	db.ref("v3/config/workshop/").ref.update({data:data,lastUpdate:new Date()})
}

exports.addApresentacao=function(data,app,callback){
	db.ref("v3/workshop/"+data).ref.push(app,function(info){
		callback(info);
	})	
}


exports.rmPalestra=function(idTecDay,idPalestra){
	db.ref("v2/tecday/"+idTecDay).ref.push(palestra);
}


exports.addWorkshop=function(data,callback){
	db.ref("v3/workshop/"+data).ref.update({dataCriacao:new Date()});
}

exports.getWorkshops=function(callback){

	db.ref("v3/workshop").ref.once('value',function(snapshot){
		var ret=[];
		snapshot.forEach(function(arg){
			ret.push(arg.key);
		})
		callback(ret)
	});
}
