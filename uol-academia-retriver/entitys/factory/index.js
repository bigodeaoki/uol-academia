"use strict";

var entity=require('../entity');

var validateArg=function(arg,nome){
	if(!isNaN(arg) || arg==null){
		throw new Error("Argumento "+nome+" invalido: "+arg);
	}
}

var PalestraFactroy=function (){
	var _self=this;


	this.Area=function(area){
		_self.area=area;
        return _self;
	}
    this.Autor=function(autor){
    	_self.autor=autor;
        return _self;
    }
    this.Hora=function(hora){
    	_self.hora=hora;
        return _self;
    }
    this.Linguagem=function(linguagem){
    	_self.linguagem=linguagem;
        return _self;
    }
    this.Resumo=function(resumo){
    	_self.resumo=resumo;
        return _self;
    }
    this.Titulo=function(titulo){
    	_self.titulo=titulo;
        return _self;
    }

    this.build=function(){
    	var palestra=new entity.Palestra();

    	validateArg(_self.area,"Area");
    	palestra.area=_self.area
    	
        validateArg(_self.autor,"Autor");
    	palestra.autor=_self.autor
    	
        validateArg(_self.hora,"Hora");
    	palestra.hora=_self.hora
    	
        validateArg(_self.linguagem,"Linguagem");
    	palestra.linguagem=_self.linguagem
    	
        validateArg(_self.resumo,"Resumo");
    	palestra.resumo=_self.resumo
        
    	
        validateArg(_self.titulo,"Titulo");
    	palestra.titulo=_self.titulo

    	return palestra;

    }
}

exports.getPalestraFactroy=function(){
    return new PalestraFactroy();
}