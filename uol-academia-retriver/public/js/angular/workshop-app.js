var app=angular
.module('academiaRetriver', ['ui.router'])
.config(function($stateProvider){

})
.controller('workshopAdicionar',function($scope,$http){
	$scope.dia=null;
	$scope.mes=null;
	$scope.ano=null;

	$scope.addWorkshop=function(){
		var data=$scope.dia+"_"+$scope.mes+"_"+$scope.ano;
		$http.post("workshop/"+data).then(function(resp){
			$scope.dia=null;
		})
	}
})
.controller('workshopController',function($scope,$http){
	$scope.workshops=[];

	$scope.update=function(){
		$http.get("workshop").then(function(resp){
			$scope.workshops=resp.data;
		})	
	}

	$scope.dia=function(arg){
  		if(angular.isDefined(arg)){
  			$scope._dia=arg;
  			$scope.setDia();
  			return $scope._dia;
  		}else{
  			return $scope._dia;
  		}
  	};

	$scope.setDia=function(){
		$http.put("config/workshop/"+$scope._dia);
	}

	$scope.update();

})
.controller('workshopAdicionarApresentacao',function($scope,$http){
	$scope.workshops=[];
	$scope._workshop=null;
	$scope.titulo=null;
	$scope.autor=null;
	$scope.hora=null;

	$scope.workshop=function(arg){
  		if(angular.isDefined(arg)){
  			$scope._workshop=arg;
  			return $scope._workshop;
  		}else{
  			return $scope._workshop;
  		}
  	};

	$scope.update=function(){
		$http.get("workshop").then(function(resp){
			$scope.workshops=resp.data;
		})		
	}
	
	$scope.addPalestra=function(){
		$scope.titulo=encodeURIComponent($scope.titulo);
		$scope.autor=encodeURIComponent($scope.autor);
		$scope.hora=encodeURIComponent($scope.hora);

		$http.post("workshop/"+$scope._workshop+"/apresentacao?"+
			"titulo="+$scope.titulo+"&autor="+$scope.autor+
			"&hora="+$scope.hora)
		.then(function(resp){
			$scope.titulo="";
			$scope.autor="";
			$scope.hora="";
			$scope.update();
			console.log(resp.data);
		})
	}
	$scope.update();
})
.controller('workshopApresentacao',function($scope){
	$scope.titulo=null;
	$scope.listaPerguntas=null;
	$scope.listaFeedback=null;



})