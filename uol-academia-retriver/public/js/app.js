var app=angular
.module('academiaRetriver', ['ui.router'])
.config(function($stateProvider){

})
.controller('tecday', function($scope,$http){

	$scope.dia=function(arg){
  		if(angular.isDefined(arg)){
  			$scope._dia=arg;
  			return $scope._dia;
  		}else{
  			return $scope._dia;
  		}
  	};

  	$scope.mes=function(arg){
  		if(angular.isDefined(arg)){
  			$scope._mes=arg;
  			return $scope._mes;
  		}else{
  			return $scope._mes;
  		}
  	};
  	$scope.ano=function(arg){
  		if(angular.isDefined(arg)){
  			$scope._ano=arg;
  			return $scope._ano;
  		}else{
  			return $scope._ano;
  		}
  	};

	var update=function(){
		$http
		.get("/tecDays")
		.then(function(resp){
			$scope.tecdays=resp.data;
		});	
	}

	$scope.adicionar=function(){
		var id=$scope.dia()+"_"+$scope.mes()+"_"+$scope.ano();
		$http.post("/tecDay/"+id+'/').then(function(){
			setTimeout(update,1000);
			$scope.dia(0);
			$scope.mes(0);
			$scope.ano(0);
		}
		);
	}

	update();	
})
.controller('palestra', function($scope,$http){
	var tecDay=null;
  var idPalestra=null;
  $scope.feedback=[];
  $scope.inscricao=[];
  $scope.users=[];
  $scope.qrcode =null;



  var getPalestraItem=function(idTecDay,idPalestra,item,callback){
    $http
    .get("/tecDay/"+idTecDay+"/palestra/"+idPalestra+"/"+item)
    .then(function(resp){
      callback(resp.data);
    });
  }

  var getTecDay=function(idTecDay,callback){
    $http
      .get("/tecDay/"+idTecDay)
      .then(function(resp){
        callback(resp.data);
      }); 
  }


  var update=function(){

    
    $http
    .get("/tecDays")
    .then(function(resp){
      $scope.tecdays=resp.data;
    }); 
    if(tecDay!=null){
      getTecDay(tecDay,function(arg){
        console.log(arg);
        $scope.tecDay=arg;
      });
    }
    if(idPalestra!=null){
      console.log("============>   "+idPalestra);
      getPalestraItem(tecDay,idPalestra,"feedback",function(arg){$scope.feedback=arg});
      getPalestraItem(tecDay,idPalestra,"users",function(arg){$scope.users=arg});
      if($scope.qrcode ==null){
        $scope.qrcode = new QRCode(document.getElementById("qrcode"), {text: "http://jindo.dev.naver.com/collie",width:75,height:75})
      }
      $scope.qrcode .clear(); // clear the code.
      $scope.qrcode .makeCode(tecDay+"/"+idPalestra); // make another code.
      //getPalestraItem(tecDay,idPalestra,"users",function(arg){$scope.feedback=arg});
    }


  }
  $scope.selectPalestra=function(arg){
      if(angular.isDefined(arg)){
        idPalestra=arg;
        update();
        return idPalestra;
      }else{
        return idPalestra;
      }
    };

  $scope.selectTecDay=function(arg){
      if(angular.isDefined(arg)){
        tecDay=arg;
        update();
        return tecDay;
      }else{
        return tecDay;
      }
    };


  update();

  $scope.sorteio = function () {

    // Comeco do sorteio
    // ler de uma lista que sera diferente
    var sorteio = function () {
      
      settings: {
        usuarios: firebase.getInscritos();
      },
      
      init: function () {
        // comecamos com o random
        var index = this.sortear( this.settings.usuarios );
        // verificaremos
        this.verificar(this.settings.usuarios[index]);

      },
      
      verificar: function (user) {
        // vamos verificar se o usuario jah existe na lista de sorteados
        var sorteados = firebase.getSorteados(user);
        if (sorteados == true) {
          // Sortearemos denovo
          this.init();
        } else {
          firebase.saveSorteado(user);
          this.terminar(user);
        }
      },
      
      sortear: function (lista) {
        
        //sortear
        var a = Math.random() * (lista.length+1);
        var index = Math.ceil(a) - 1;

        return index;

      },
      
      terminar: function (user) {
        alert(user.nome);
      }


    }
    // depois de ler sortear, elementos
    
    // user sorteado
    var user = lista[index];
    // gravar no firebase, apra podemos consultar se elemento jah foi sorteado
    firebase.getSorteados(user);
    // se jah foi sorteado, realizar novamente

    // se nao, exibir nome do sorteado


  }

})

// $stateProvider, $urlRouterProvider