"use strict";
var firebase=require("../firebase");
var factory= require("../entitys/factory");
// Teste da Home
exports.getTecDays = function (req,res) {		
    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        res.send(arg);  
    }
    firebase.getTecDays(callback);
}

exports.getTecDay=function(req,res){
    var id=req.params.id;
    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        res.send(arg);  
    }
    firebase.getTecDay(id,callback);
}

exports.getPalestra=function(req,res){
    var idTecDay=req.params.idTecDay;
    var idPalestra=req.params.idPalestra;
    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        res.send(arg);  
    }
    firebase.getPalestra(idTecDay,idPalestra,callback);
}

exports.getPalestraFeedback=function(req,res){
    var idTecDay=req.params.idTecDay;
    var idPalestra=req.params.idPalestra;
    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        res.send(arg);  
    }
    firebase.getPalestraFeedback(idTecDay,idPalestra,callback);   
}


exports.getPalestraUsers=function(req,res){
    var idTecDay=req.params.idTecDay;
    var idPalestra=req.params.idPalestra;
    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        res.send(arg);  
    }
    firebase.getPalestraUsers(idTecDay,idPalestra,callback);
}

exports.addTecDay=function(req,res){
    var idTecDay=req.params.idTecDay;
    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        if(arg==null){
            res.status(304);
        }else{
            res.status(201);
        }

        res.send(arg);
    }

    firebase.addTecDay(idTecDay,callback);
}

exports.addWorkshop=function(req,res){
    var data=req.params.data;
    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        if(arg==null){
            res.status(304);
        }else{
            res.status(201);
        }

        res.send(arg);
    }

    firebase.addWorkshop(data,callback);
}

exports.getWorkshops=function(req,res){
     var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        res.send(arg);
    }

    firebase.getWorkshops(callback);
}

exports.setWorkshop=function(req,res){
    var data=req.params.data;
    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        res.send(arg);
    }
    firebase.setWorkshop(data,callback);
}

exports.addApresentacao=function(req,res){
    var data=req.params.data;
    var app={
    titulo:req.query.titulo
    ,autor:req.query.autor
    ,hora:req.query.hora}   

    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        if(arg==null){
            res.status(201);
        }else{
            res.status(304);
        }

        res.send(arg);
    }

    firebase.addApresentacao(data,app,callback);
    

}



exports.addPalestra=function(req,res){
    var idTecDay=req.params.idTecDay;
    var area=req.body.area;
    var autor=req.body.autor;
    var hora=req.body.hora;
    var linguagem=req.body.linguagem;
    var resumo=req.body.resumo;
    var titulo=req.body.titulo;

    var palesta=factory.getPalestraFactroy()
    .Area(area)
    .Autor(autor)
    .Hora(hora)
    .Linguagem(linguagem)
    .Resumo(resumo)
    .Titulo(titulo)
    .build();

    var callback=function(arg){
        res.setHeader('Content-Type', 'application/json');
        res.send(arg);
    }

    firebase.addPalestra(idTecDay,palesta,callback);

    
    
}