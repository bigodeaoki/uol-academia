var HashTable = require('hashtable');
var express = require("express");
var firebase = require("./firebase");
var routing = require("./routing");
var app = express();




var palestrasMap = new HashTable();
var portnumber = 3100;

app.use(express.json());    
app.use(express.urlencoded());

app.get('/tecDays', routing.getTecDays);
app.get('/tecDay/:id', routing.getTecDay);
app.get('/tecDay/:idTecDay/palestra/:idPalestra', routing.getPalestra);
app.get('/tecDay/:idTecDay/palestra/:idPalestra/feedback', routing.getPalestraFeedback);
app.get('/tecDay/:idTecDay/palestra/:idPalestra/users', routing.getPalestraUsers);
app.get('/workshop', routing.getWorkshops);

app.post('/workshop/:data',routing.addWorkshop)
app.post('/workshop/:data/apresentacao',routing.addApresentacao)


app.post('/tecDay/:idTecDay/', routing.addTecDay);
app.post('/tecDay/:idTecDay/palestra/', routing.addPalestra);



app.put('/config/workshop/:data',routing.setWorkshop);

app.use(express.static('./public'));


console.log("Start Running Server");
app.listen(portnumber);
console.log("OK! Running Server");
console.log("Running on port " + portnumber);
